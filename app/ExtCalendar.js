/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.ExtCalendar', {
    extend: 'Ext.ux.desktop.Module',

    requires: [],

    id: 'extcalendar',

    init: function () {
        this.launcher = {
            text: 'ExtCalendar',
            iconCls: 'extcalendar'
        };
    },
    something: function () {

    },

    createWindow: function () {
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('extcalendar');
        if (!win) {
            win = desktop.createWindow({
                id: 'extcalendar',
                title: 'ExtCalendar',
                width: 600,
                height: 400,
                iconCls: 'extcalendar',
                animCollapse: false,
                border: false,
                //defaultFocus: 'notepad-editor', EXTJSIV-1300

                // IE has a bug where it will keep the iframe's background visible when the window
                // is set to visibility:hidden. Hiding the window via position offsets instead gets
                // around this bug.
                hideMode: 'offsets',

                layout: 'fit',
                items: [
                    {
                        xtype: 'component',
                        autoEl: {
                            tag: 'iframe',
                            src: "https://examples.sencha.com/extjs/6.2.0/examples/calendar/"
                        }
                    }
                ]
            });
        }
        return win;
    }
});
