/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.TabWindow', {
    extend: 'Ext.ux.desktop.Module',

    requires: [
        'Ext.tab.Panel'
    ],

    id: 'tab-win',

    init: function () {
        this.launcher = {
            text: 'Tab Window',
            iconCls: 'tabs'
        }
    },

    createWindow: function () {
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('tab-win');
        if (!win) {
            win = desktop.createWindow({
                id: 'tab-win',
                title: 'Tab Window',
                width: 740,
                height: 480,
                iconCls: 'tabs',
                animCollapse: false,
                border: false,
                constrainHeader: true,

                layout: 'fit',
                items: [
                    {
                        xtype: 'tabpanel',
                        activeTab: 0,
                        bodyStyle: 'padding: 5px;',
                        items: [
                            {
                                title: 'Tab Cartesian',
                                xtype: 'cartesian',
                                innerPadding: {
                                    top: 50,
                                    left: 10,
                                    right: 10,
                                    bottom: 0
                                },
                                store: {
                                    fields: ['text', 'value'],
                                    data: [{
                                        text: 'test 1',
                                        value: 10
                                    }, {
                                        text: 'test 2',
                                        value: 20
                                    }, {
                                        text: 'test 3',
                                        value: 30
                                    }, {
                                        text: 'test 4',
                                        value: 40
                                    }, {
                                        text: 'test 5',
                                        value: 30
                                    }, {
                                        text: 'test 6',
                                        value: 20
                                    }]
                                },
                                axes: [{
                                    type: 'category',
                                    position: 'bottom'
                                }, {
                                    type: 'numeric',
                                    position: 'left'
                                }],
                                series: {
                                    type: 'bar',
                                    xField: 'text',
                                    yField: 'value',
                                    label: {
                                        field: 'value'
                                    }
                                }
                            },
                            {
                                title: 'Tab Another Cartesian',
                                xtype: 'cartesian',
                                innerPadding: {
                                    top: 50,
                                    left: 10,
                                    right: 10,
                                    bottom: 0
                                },
                                store: {
                                    fields: ['text', 'value'],
                                    data: [{
                                        text: 'test 1',
                                        value: 10
                                    }, {
                                        text: 'test 2',
                                        value: 20
                                    }, {
                                        text: 'test 3',
                                        value: 30
                                    }, {
                                        text: 'test 4',
                                        value: 40
                                    }, {
                                        text: 'test 5',
                                        value: 30
                                    }, {
                                        text: 'test 6',
                                        value: 20
                                    }]
                                },
                                axes: [{
                                    type: 'category',
                                    position: 'bottom'
                                }, {
                                    type: 'numeric',
                                    position: 'left'
                                }],
                                series: {
                                    type: 'bar',
                                    xField: 'value',
                                    yField: 'text',
                                    label: {
                                        field: 'text'
                                    }
                                }
                            }, {
                                title: 'Tab Text 3',
                                header: false,
                                html: '<p>Something useful would be in here.</p>',
                                border: false
                            }, {
                                title: 'Tab Text 4',
                                header: false,
                                html: '<p>Something useful would be in here.</p>',
                                border: false
                            }]
                    }
                ]
            });
        }
        return win;
    }
});
