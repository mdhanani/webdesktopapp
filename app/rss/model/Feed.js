Ext.define('MyApp.rss.model.Feed', {
    extend: 'MyApp.rss.model.Base',

    requires: [
        'MyApp.rss.model.Entry'
    ],

    fields: [
        'title', 'author', 'link', 'description', 'feedUrl', 'type'
    ],

    hasMany: {
        model: 'MyApp.rss.model.Entry',
        name: 'entries'
    },

    proxy: {
        type: 'googglerss'
    },

    populateEntries: function(callback) {
        var me = this;

        if (me.loaded) {
            return;
        }

        // Start loading the feed's entries
        me.load({
            url: me.get('feedUrl'),
            limit: 10,
            callback: function (record, operation, success) {
                me.loaded = success;
                if (callback) {
                    callback(success);
                }
            }
        });
    }
});
