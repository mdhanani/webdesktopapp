Ext.define('MyApp.rss.model.Entry', {
    extend: 'MyApp.rss.model.Base',

    isRssEntry: true,

    fields: [
        'title',
        'author',
        'link',
        'categories',
        'url',
        {
            name: 'publishedDate',
            type: 'date',
            dateFormat : 'c'
        },
        'content',
        'contentSnippet'
    ]
});
