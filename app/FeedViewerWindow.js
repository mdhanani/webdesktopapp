/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.FeedViewerWindow', {
    extend: 'Ext.ux.desktop.Module',

    id: 'feedwindow',

    init: function () {
        this.launcher = {
            text: 'Feed Viewer',
            iconCls: 'feedwindow'
        };
    },
    createWindow: function () {
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('feedwindow');
        if (!win) {
            win = desktop.createWindow({
                id: 'feedwindow',
                title: 'Feed Viewer',
                width: 600,
                height: 400,
                iconCls: 'feedwindow',
                animCollapse: false,
                border: false,
                //defaultFocus: 'notepad-editor', EXTJSIV-1300

                // IE has a bug where it will keep the iframe's background visible when the window
                // is set to visibility:hidden. Hiding the window via position offsets instead gets
                // around this bug.
                hideMode: 'offsets',

                layout: 'fit',
                items: [{
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        src: "http://cdn.sencha.com/ext/gpl/5.1.0/build/examples/feed-viewer/feed-viewer.html?theme=neptune"
                    }
                }]
            })
        }
        return win;
    }
});
