/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.FileBrowserApp', {
    extend: 'Ext.ux.desktop.Module',

    requires: [
        'MyApp.filebrowser.FileBrowser'
    ],

    id:'filebrowserapp',

    init : function(){
        this.launcher = {
            text: 'File Browser',
            iconCls:'filebrowserapp'
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('filebrowserapp');
        if(!win){
            win = desktop.createWindow({
                id: 'filebrowserapp',
                title:'File Browser',
                width:600,
                height:400,
                iconCls: 'notepad',
                animCollapse:false,
                border: false,
                hideMode: 'offsets',

                layout: 'fit',
                items: [
                    {
                        xtype: 'filebrowser',
                        address: {
                            urlGet: serverAddress+'fss/directory',
                            urlNewFile: serverAddress+'fss/file',
                            urlNewDirectory: serverAddress+'fss/directory',
                            urlRename: serverAddress+'fss/directory',
                            urlDelete: serverAddress+'fss/directory',
                            urlCpmv: serverAddress+'fss/cpmv',
                            urlViewFile: serverAddress+'fss/file',
                            urlSave: serverAddress+'fss/file'
                        }
                    }
                ]
            });
        }
        return win;
    }
});
