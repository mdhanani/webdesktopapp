/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.FullCalendarWindow', {
    extend: 'Ext.ux.desktop.Module',

    id: 'fullcalendar',

    init: function () {
        this.launcher = {
            text: 'Full Calendar',
            iconCls: 'fullcalendar'
        };
    },
    createWindow: function () {
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('fullcalendar');
        if (!win) {
            win = desktop.createWindow({
                id: 'fullcalendar',
                title: 'Full Calendar',
                width: 600,
                height: 400,
                iconCls: 'fullcalendar',
                animCollapse: false,
                border: false,
                //defaultFocus: 'notepad-editor', EXTJSIV-1300

                // IE has a bug where it will keep the iframe's background visible when the window
                // is set to visibility:hidden. Hiding the window via position offsets instead gets
                // around this bug.
                hideMode: 'offsets',

                layout: 'fit',
                items: [{
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        src: "resources/selectable-calendar.html"
                    }
                }]
            })
        }
        return win;
    }
});
