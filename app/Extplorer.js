/*!
 * Ext JS Library
 * Copyright(c) 2006-2014 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

Ext.define('Desktop.Extplorer', {
    extend: 'Ext.ux.desktop.Module',

    requires: [
        'Ext.ux.IFrame'
    ],

    id: 'extplorer',

    init: function () {
        this.launcher = {
            text: 'Extplorer',
            iconCls: 'extplorer'
        };
    },

    createWindow: function () {
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('extplorer');
        if (!win) {
            win = desktop.createWindow({
                id: 'extplorer',
                title: 'Extplorer',
                width: 740,
                height: 480,
                iconCls: 'extplorer',
                animCollapse: false,
                constrainHeader: true,
                layout: 'fit',
                items: [{
                    xtype: 'component',
                    autoEl: {
                        tag: 'iframe',
                        src: 'http://localhost:8088/wfm'
                    }
                }]
            });
        }
        return win;
    },
});

